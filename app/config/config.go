package config

import "os"

var (
	ProjectRoot string
)

func init() { ProjectRoot = os.Getenv("PROJECT_ROOT") }
