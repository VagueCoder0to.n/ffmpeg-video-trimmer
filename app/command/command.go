package command

import (
	"fmt"
	"log"
	"strings"

	"github.com/banaqerja/cinema"
	"gitlab.com/VagueCoder0to.n/FFmpeg-Video-Trimmer/app/inputs"
)

func GetCommand(i *inputs.Inputs) (string, error) {
	video, err := cinema.Load(i.GetInputFileName())
	if err != nil {
		log.Fatal(err)
	}

	start := i.GetStartMinutes() + i.GetStartSeconds()
	end := i.GetEndMinutes() + i.GetEndSeconds()

	if start == 0 && end == 0 {
		return "", fmt.Errorf("no start and end time filters found")
	} else {
		if start != 0 {
			video.SetStart(start)
		}
		if end != 0 {
			video.SetEnd(end)
		}
	}

	return strings.Join(video.CommandLine(i.GetOutputFileName()), " "), nil
}
