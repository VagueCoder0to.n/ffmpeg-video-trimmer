package inputs

import (
	"time"

	"gitlab.com/VagueCoder0to.n/FFmpeg-Video-Trimmer/app/logger"
)

type Inputs struct {
	*logger.Logger

	inputFile  string
	outputFile string
	start      *timeSet
	end        *timeSet
}

type timeSet struct {
	minutes time.Duration
	seconds time.Duration
}

type inputOptions func(*Inputs)

func NewInputs(logger *logger.Logger, options ...inputOptions) *Inputs {
	i := &Inputs{
		Logger:     logger,
		inputFile:  "",
		outputFile: "output.mkv",
		start: &timeSet{
			minutes: 0,
			seconds: 0,
		},
		end: &timeSet{
			minutes: 0,
			seconds: 0,
		},
	}

	for _, fun := range options {
		fun(i)
	}

	if i.inputFile == "" {
		i.Fatal("no input file name specified.")
	}

	return i
}

func AddInputFile(name string) inputOptions {
	return func(i *Inputs) {
		i.inputFile = name
	}
}

func AddOutputFile(name string) inputOptions {
	return func(i *Inputs) {
		i.outputFile = name
	}
}

func AddStartTime(m, s int) inputOptions {
	return func(i *Inputs) {
		i.start = &timeSet{
			minutes: time.Duration(m) * time.Minute,
			seconds: time.Duration(s) * time.Second,
		}
	}
}

func AddEndTime(m, s int) inputOptions {
	return func(i *Inputs) {
		i.end = &timeSet{
			minutes: time.Duration(m) * time.Minute,
			seconds: time.Duration(s) * time.Second,
		}
	}
}

func (i *Inputs) GetInputFileName() string {
	return i.inputFile
}

func (i *Inputs) GetOutputFileName() string {
	return i.outputFile
}

func (i *Inputs) GetStartMinutes() time.Duration {
	return i.start.minutes
}

func (i *Inputs) GetStartSeconds() time.Duration {
	return i.start.seconds
}

func (i *Inputs) GetEndMinutes() time.Duration {
	return i.end.minutes
}

func (i *Inputs) GetEndSeconds() time.Duration {
	return i.end.seconds
}
