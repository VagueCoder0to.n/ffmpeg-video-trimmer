package logger

import (
	"fmt"
	"log"
	"os"
)

type Logger struct {
	*log.Logger
}

func NewLogger(prefix string) *Logger {
	logger := &Logger{
		Logger: log.New(os.Stderr,
			fmt.Sprintf("[%s] ", prefix),
			log.Lshortfile|log.LstdFlags),
	}

	return logger
}

func (l *Logger) Print(args ...interface{}) {
	args[0] = "INFO: " + args[0].(string)
	l.Println(args...)
}

func (l *Logger) Fatal(args ...interface{}) {
	args[0] = "ERROR: " + args[0].(string)
	l.Fatalln(args...)
}
