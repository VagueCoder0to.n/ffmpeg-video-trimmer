package main

import (
	"fmt"
	"log"

	"gitlab.com/VagueCoder0to.n/FFmpeg-Video-Trimmer/app/command"
	"gitlab.com/VagueCoder0to.n/FFmpeg-Video-Trimmer/app/inputs"
	"gitlab.com/VagueCoder0to.n/FFmpeg-Video-Trimmer/app/logger"
)

func main() {
	l := logger.NewLogger("Video-Split-Trim")
	l.Print("FFmpeg is a prerequisite. Application only gives the command to execute with FFmpeg.\n")

	ips := inputs.NewInputs(
		l,

		inputs.AddInputFile("Workout.mkv"),
		inputs.AddOutputFile("test_output.mkv"),
		inputs.AddStartTime(1, 22),
		inputs.AddEndTime(3, 38),
	)

	cmd, err := command.GetCommand(ips)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Command to run:\n%s\n", cmd)
}
